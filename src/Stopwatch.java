/*  *************************************************************************
 ** Stopwatch is a fullscreen stopwatch and countdown timer.
 ** 
 ** Copyright (C) 2018 Jérémy "Le JyBy" Barbay
 ** from sources orginally from Copyright (C) 2005 Jess Thrysoee
 ** 
 ** This program is free software; you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation; either version 2 of the License, or
 ** (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 ** GNU General Public License for more details.
 ** 
 ** You should have received a copy of the GNU General Public License
 ** along with this program; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 **
 ** *************************************************************************/

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;
import java.util.prefs.Preferences;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import java.net.URL;
import java.net.MalformedURLException;
import javax.swing.JOptionPane;
import java.awt.Component;
import javax.swing.JComponent;
import java.util.Date; // To be able to specify the end time of a class on the command line, and have the stopwatch starting immediately
import java.util.*; // To parse date given as parameter
import java.text.*; // To parse date given as parameter

/**
 * Stopwatch is a fullscreen stopwatch and countdown timer
 */
public class Stopwatch extends JPanel {

   public String appName;

   JFrame frame;
   JPopupMenu popup;

   JMenuItem startStopItem;
   JMenuItem splitTimeItem;

   ConfigDialog configDialog;
   Preferences prefs;

   boolean fullscreen = false; // true if in fullscreen mode
   static final Dimension FULLSCREEN_DIM = Toolkit.getDefaultToolkit().getScreenSize();

   Action fullScreenAction,  // the actions used for popup and keys
          startStopAction,
          splitTimeAction,
          resetAction,
          configureAction,
          quitAction;
   
   boolean backgroundColon = false;  // true if split time colon is background color

   Timer timer = new Timer();   // Timer firing every half second
   TimerTask timerTask = null;

   MaxSizeLabel timeStringLabel;  // the maximized label
   int sign = 1;   // counting up(stopwatch) or down

   DecimalFormat twoDigits = new DecimalFormat("00");

   boolean splitTime = false; //  true if in split time mode
   long splitTimeSeconds;    // the second split time started

   static long startValue = 25*60; // 25 mns = 1 Pomodoro
   // static long startValue = 1*60*60+30*60; // 1h30mns of class
   long seconds = 0;  // the seconds since start

   // default values and keys
   private static final String startSecondKey = "StartSecond";
   long startSecond = 0;  // initially start from, and reset to, this second

   private static final String foregroundKey = "Foreground";
   Color curForeground = Color.black;
   private static final String backgroundKey = "Background";
   Color curBackground = Color.white;

   private static final String fontFamilyKey = "FontFamily";
   Font curFont; // current font used for timeStringLabel
    
   private static final String audioClipMuteKey = "AudioMute";
   boolean curAudioMute; // play sound when count down reach zero

   private static final String audioClipURLKey = "AudioURL";
   String curAudioURLString = "http://www.thrysoee.dk/stopwatch/gong.wav";
   AudioHandler curAudio; // syncronized AudioClip wrapper


   /**
    * Constructor
    */
    public Stopwatch(JFrame frame) {
      super(new GridLayout(), true);

      this.frame = frame;
      this.appName = getClass().getName();

      timeStringLabel = new MaxSizeLabel(getTimeString(seconds), JLabel.CENTER);
      add(timeStringLabel);

      setPreferences();

      createActions();
      createPopupMenu();
      MouseListener popupListener = new PopupListener();
      addMouseListener(popupListener);

      configDialog = new ConfigDialog(frame, this);
   }


   /**
    * set saved preferences or default values
    */
   private void setPreferences() {

      prefs = Preferences.userNodeForPackage(this.getClass());

      setStartSecond(prefs.getLong(startSecondKey, startSecond));
      seconds = startSecond;

      setCurBackground(new Color(prefs.getInt(backgroundKey, curBackground.getRGB())));
      setCurForeground(new Color(prefs.getInt(foregroundKey, curForeground.getRGB())));
      setCurFontFamily(prefs.get(fontFamilyKey, "Default"), false);

      setCurAudioURLString(prefs.get(audioClipURLKey, curAudioURLString));
      curAudio = new AudioHandler();
      try {
         curAudio.load(new URL(curAudioURLString));
      } catch(MalformedURLException e) { }

      setCurAudioMute(prefs.getBoolean(audioClipMuteKey, true));
   }


   /**
    * Create actions for popup and keys
    */
   public void createActions() {

      fullScreenAction = new FullScreenAction();
      getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
          put(KeyStroke.getKeyStroke('f'), "fullScreenAction");
      getActionMap().put("fullScreenAction", fullScreenAction);

      startStopAction = new StartStopAction();
      getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
          put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0), "startStopAction");
      getActionMap().put("startStopAction", startStopAction);

      splitTimeAction = new SplitTimeAction();
      getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
          put(KeyStroke.getKeyStroke('p'), "splitTimeAction");
      getActionMap().put("splitTimeAction", splitTimeAction);

      resetAction = new ResetAction();
      getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
          put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "resetAction");
      getActionMap().put("resetAction", resetAction);

      configureAction = new ConfigureAction();
      getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
          put(KeyStroke.getKeyStroke('c'), "configureAction");
      getActionMap().put("configureAction", configureAction);

      quitAction = new QuitAction();
      getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
          put(KeyStroke.getKeyStroke('q'), "quitAction");
      getActionMap().put("quitAction", quitAction);
   }


   /**
    * Create the popup menu.
    */
   public void createPopupMenu() {

      popup = new JPopupMenu();
      JMenuItem menuItem;

      menuItem = new JMenuItem(fullScreenAction);
      menuItem.setText("Full Screen");
      popup.add(menuItem);

      popup.addSeparator();

      startStopItem = new JMenuItem(startStopAction);
      startStopItem.setText("Start");
      popup.add(startStopItem);

      splitTimeItem = new JMenuItem(splitTimeAction);
      splitTimeItem.setText("Split Time");
      splitTimeItem.setEnabled(false);
      popup.add(splitTimeItem);

      menuItem = new JMenuItem(resetAction);
      menuItem.setText("Reset");
      popup.add(menuItem);

      popup.addSeparator();

      menuItem = new JMenuItem(configureAction);
      menuItem.setText("Configure...");
      popup.add(menuItem);

      popup.addSeparator();

      menuItem = new JMenuItem(quitAction);
      menuItem.setText("Quit");
      popup.add(menuItem);

      start();
      
   }


   /**
    * Start timer
    */
   public void start() {

      // determine countdown/up 
      if (seconds == startSecond) {
         sign = seconds > 0 ? -1 : 1;
      }

      // reset if starting after finished countdown
      if (seconds >= 0 && startSecond > 0) {
         seconds = startSecond;
      }

      timeStringLabel.setText(getTimeString(seconds));

      timerTask = new StopwatchTimerTask();
      timer.scheduleAtFixedRate(timerTask, 500, 500);

      startStopItem.setText("Stop");
      splitTimeItem.setEnabled(true);
   }


   /**
    * Stop timer
    */
   public long stop() {
      if (splitTime) {
         splitTime = false;
         timeStringLabel.setText(getTimeString(seconds));
      }
      timerTask.cancel();
      timerTask = null;

      startStopItem.setText("Start");
      splitTimeItem.setEnabled(false);
      return seconds;
   }


   /**
    * Get HTML hex representation of Color, e.g. #FF00FF
    */
   public String colorToHex(Color c)
   {
      String s = Integer.toHexString(c.getRGB() & 0xFFFFFF);
      return ("#" + "000000".substring(s.length()) + s.toUpperCase());
   }


   /**
    * Get HTML formated time string
    */
   String getTimeString(long sec) {

      String colonColor = colorToHex(curForeground);

      // if splitTime always show splitTimeSeconds and blink colons
      if (splitTime) {
         if (backgroundColon) {
            colonColor = colorToHex(curBackground);
            backgroundColon = false;
         } else {
            backgroundColon = true;
         }

         sec = splitTimeSeconds;
      }

      return "<html>"
         + twoDigits.format(sec/3600)
         + "<font color=" + colonColor + ">:</font>"
         + twoDigits.format((sec%3600)/60)
         + "<font color=" + colonColor + ">:</font>"
         + twoDigits.format((sec%3600)%60);
   }


   /**
    * Accessor get FontFamily
    */
   public String getCurFontFamily() {
      return curFont.getFamily();
   }

   /**
    * Accessor set FontFamily
    */
   public void setCurFontFamily(String fontFamily, boolean resize) {

      curFont = new Font(fontFamily, Font.BOLD, 12);

      timeStringLabel.setFont(curFont);

      if (resize) {
         timeStringLabel.resize();
      }

      prefs.put(fontFamilyKey, fontFamily);
   }

   /**
    * Accessor get foreground color
    */
   public Color getCurForeground() {
      return curForeground;
   }

   /**
    * Accessor set foreground color
    */
   public void setCurForeground(Color color) {
      curForeground = color;

      timeStringLabel.setForeground(curForeground);

      // update HTML color attributes
      timeStringLabel.setText(getTimeString(seconds));

      prefs.putInt(foregroundKey, curForeground.getRGB());
   }

   /**
    * Accessor get background color
    */
   public Color getCurBackground() {
      return curBackground;
   }

   /**
    * Accessor set background color
    */
   public void setCurBackground(Color color) {
      curBackground = color;

      setBackground(curBackground);
      prefs.putInt(backgroundKey, curBackground.getRGB());
   }

   /**
    * Accessor get start second
    */
   public long getStartSecond() {
      return startSecond;
   }

   /**
    * Accessor set start second
    */
   public void setStartSecond(long sec) {
      startSecond = sec;
      prefs.putLong(startSecondKey, startSecond);
   }

   /**
    * Accessor get sound on/off setting
    */
   public boolean getCurAudioMute() {
      return curAudioMute;
   }

   /**
    * Accessor set sound on/off setting
    */
   public void setCurAudioMute(boolean mute) {
      curAudioMute = mute;
      prefs.putBoolean(audioClipMuteKey, mute);
   }

   /**
    * Accessor get audio clip URL
    */
   public String getCurAudioURLString() {
      return curAudioURLString;
   }

   /**
    * Accessor set audio clip URL
    */
   public void setCurAudioURLString(String urlString) {

      curAudioURLString = urlString.trim();
      prefs.put(audioClipURLKey, curAudioURLString);
   }

   /**
    * Accessor copy curAudio to 'dst' AudioHandler
    */
   public void getCurAudio(AudioHandler dst) {
      curAudio.copyTo(dst);
   }

   /**
    * Accessor copy 'src' AudioHandler to curAudio
    */
   public void setCurAudio(AudioHandler src) {
      src.copyTo(curAudio);
   }

   /**
    * play sound when count down reach zero
    */
   public void playAudio() {

      if (curAudioMute == true) {
         curAudio.play();
      }
   }


   /**
    * Create GUI on the event-dispatching thread
    */
   static void createAndShowGUI() {

      try {
         UIManager.setLookAndFeel(
               UIManager.getSystemLookAndFeelClassName());
      } catch (Exception e) { }

      // live resize
      Toolkit.getDefaultToolkit().setDynamicLayout(true);

      JFrame frame = new JFrame("Stopwatch");
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      Stopwatch newContentPane = new Stopwatch(frame);
      newContentPane.setOpaque(true);
      frame.setContentPane(newContentPane);

      // Setting the first time
      newContentPane.seconds = Stopwatch.startValue;
      
      // Positioning the stopwatch
      // frame.setLocationRelativeTo(null); // Centering the stopwatch
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      int width = (int) screenSize.getWidth();
      int height = (int) screenSize.getHeight();
      frame.setLocation(width-200,0); // Putting the stopwatch to the top left corner
      frame.setSize(200,70);
      frame.setVisible(true);
   }


   /**
    * TimerTask called every half second
    */
   class StopwatchTimerTask extends TimerTask {
      
      int mod = 1;

      public void run() {
         if ((mod%=2) == 0) {
            seconds += sign;
         } 
         // update Swing on the event-dispatching thread
         SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                  timeStringLabel.setText(getTimeString(seconds));
            }
         });
         ++mod;

	 // Starts counting up after finishing countdown
         if (seconds == 0) {
            playAudio();
	 }
         if (sign < 0 && seconds == 0) {
	    sign = +1;
	 }

	 if(sign <0) {
	    curBackground = Color.white;
	    setBackground(curBackground);
	 } else {
	    curBackground = Color.red;
	    setBackground(curBackground);
	 }	 
      }
   }


   /**
    * Toggle full screen ('f')
    */
   class FullScreenAction extends AbstractAction {

      Dimension frameDim;
      Point frameLocation;

      public FullScreenAction() {
         putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke('f'));
      }

      public void actionPerformed(ActionEvent e) {

         // Save decorated pos/size
         if (!fullscreen) {
            frameDim = frame.getSize();
            frameLocation= frame.getLocation();
         }
         frame.dispose();

         if (fullscreen) {
            frame.setUndecorated(false);
            frame.setResizable(true);
            frame.setSize(frameDim);
            frame.setLocation(frameLocation);
            fullscreen = false;
         } else {
            frame.setUndecorated(true);
            frame.setResizable(false);
            frame.setSize(FULLSCREEN_DIM);
            frame.setLocation(0, 0);
            fullscreen = true;
         }
         frame.setVisible(true);
      }
   }


   /**
    * Start/Stop timer ('space')
    */
   class StartStopAction extends AbstractAction {
      public StartStopAction() {
         putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0));
      }
      public void actionPerformed(ActionEvent e) {
         if (timerTask != null) {
            stop();
         } else {
            start();
         }
      }
   }


   /**
    * Pause for a split time ('p')
    */
   class SplitTimeAction extends AbstractAction {
      public SplitTimeAction() {
         putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke('p'));
      }
      public void actionPerformed(ActionEvent e) {
         if (timerTask != null) {
            if (splitTime) {
               splitTime = false;
            } else {
               splitTime = true;
               backgroundColon = true;
               splitTimeSeconds = seconds;
            }
            timeStringLabel.setText(getTimeString(seconds));
         }
      }
   }


   /**
    * Reset to start time ('esc')
    */
   class ResetAction extends AbstractAction {
      public ResetAction() {
         putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
      }
      public void actionPerformed(ActionEvent e) {
         if (timerTask != null) {
            stop();
         }
	 // reset colors
	 curBackground = Color.white;
	 setBackground(curBackground);

         // reset to configured start time
         seconds = startSecond;
         timeStringLabel.setText(getTimeString(seconds));
      }
   }


   /**
    * Show configuration dialog ('c')
    */
   class ConfigureAction extends AbstractAction {
      public ConfigureAction() {
         putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke('c'));
      }
      public void actionPerformed(ActionEvent e) {
         if (!configDialog.isVisible()) {
            configDialog.pack();
            configDialog.setVisible(true);
         } else {
            configDialog.requestFocus();
         }
      }
   }


   /**
    * Quit
    */
   static class QuitAction extends AbstractAction {
      public QuitAction() {
         putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke('q'));
      }
      public void actionPerformed(ActionEvent e) {
         System.exit(0);
      }
   }


   /**
    * PopupListener
    */
   class PopupListener extends MouseAdapter {
      public void mousePressed(MouseEvent e) {
         showPopup(e);
      }

      public void mouseReleased(MouseEvent e) {
         showPopup(e);
      }

      private void showPopup(MouseEvent e) {
         if (e.isPopupTrigger()) {
            popup.show(e.getComponent(), e.getX(), e.getY());
         }
      }
   }


   /**
    * Main
    */
   public static void main(String[] args) {
       System.out.println("\nStopwatch, from Jess sources, modified by JyBy. "
			  +"\nLast update on [2018-08-13 Mon 22:50]."); 

       if(args.length >0) {
	   SimpleDateFormat ft = new SimpleDateFormat ("HH:mm"); 
	   String input = args[0];
	   Date parameterTime;
	   try {
	       parameterTime = ft.parse(input); 
	   } catch (ParseException e) { 
	       System.out.print(input + "Unparseable using " + ft);
	       parameterTime = new Date();
	   }
	   Date currentTime = new Date();
	   
	   try {
	       String currentTimeString =  ft.format(currentTime);
	       String parameterTimeString = ft.format(parameterTime);
	       currentTime = ft.parse(currentTimeString); 
	       parameterTime = ft.parse(parameterTimeString); 
	   } catch (ParseException e) { 
	       System.out.print(input + "Unparseable using " + ft);
	       currentTime = new Date();
	       parameterTime = new Date();
	   }
	   
	   System.out.println("Current Time: " + ft.format(currentTime));
	   System.out.println("Parameter Time: "+ ft.format(parameterTime));	   

	   Stopwatch.startValue = (parameterTime.getTime() - currentTime.getTime()) / 1000;
	   if(Stopwatch.startValue<0) {
	       Stopwatch.startValue += 60*60*24; // Add one day if necessary
	   }
	   System.out.println("Difference in seconds is: "+ Stopwatch.startValue);
       } 

      // Create GUI on the event-dispatching thread
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            createAndShowGUI();
         }
      });

   }

}

