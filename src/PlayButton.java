/*  *************************************************************************
 ** Stopwatch is a fullscreen stopwatch and countdown timer.
 ** 
 ** Copyright (C) 2005 Jess Thrysoee
 ** 
 ** This program is free software; you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation; either version 2 of the License, or
 ** (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 ** GNU General Public License for more details.
 ** 
 ** You should have received a copy of the GNU General Public License
 ** along with this program; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 **
 ** *************************************************************************/

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import javax.swing.Icon;
import javax.swing.JButton;


/**
 * JButton with play symbol
 */
public class PlayButton extends JButton {

   public PlayButton() {
      super();

      setIcon(new PlayIcon());
   }

   /**
    * paint icons as triangle (play symbol)
    */
   static class PlayIcon implements Icon { 

      public int getIconWidth() { 
         return 6; 
      } 

      public int getIconHeight() { 
         return 12; 
      } 

      public void paintIcon(Component c, Graphics g, int x, int y) { 
         g.setColor(Color.black); 
         int xcoords[] = { x, x + getIconWidth(), x, x };
         int ycoords[] = { y, y + (getIconHeight()/2), y+getIconHeight(), y };
         int pts = xcoords.length;
         g.fillPolygon(xcoords,ycoords,pts);

      } 
   }

}

