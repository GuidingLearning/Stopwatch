/*  *************************************************************************
 ** Stopwatch is a fullscreen stopwatch and countdown timer.
 ** 
 ** Copyright (C) 2005 Jess Thrysoee
 ** 
 ** This program is free software; you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation; either version 2 of the License, or
 ** (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 ** GNU General Public License for more details.
 ** 
 ** You should have received a copy of the GNU General Public License
 ** along with this program; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 **
 ** *************************************************************************/

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.RenderingHints;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.Rectangle2D;
import javax.swing.JLabel;


/**
 * Max sized JLabel
 */
public class MaxSizeLabel extends JLabel {

   /**
    * Constructor
    */
   public MaxSizeLabel(String text, int horizontalAlignment) {
      super(text, horizontalAlignment);

      addComponentListener(new LabelResized());
   }


   /**
    * Antialias
    */
   public void paint(Graphics g) {
      Graphics2D g2 = (Graphics2D)g;
      g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
            RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS,
            RenderingHints.VALUE_FRACTIONALMETRICS_ON);
      super.paint(g);
   }


   /**
    * resize label to max fontsize
    */
   public void resize() {

      // remove html, it screws up the dimension calcs
      // NOTE: specific to timeStringLabel in Stopwatch
      String text = getText()
         .replaceFirst("<html>","")
         .replaceAll("<font color=#[0-9A-F]{6}>:</font>",":");

      // dimensions are negative initially!?!?
      int labelWidth = Math.abs(getWidth());
      int labelHeight = Math.abs(getHeight());

      Font font = getFont();
      float currentPointSize = font.getSize2D(); 

      FontMetrics fontMetrics = getFontMetrics(font);
      Rectangle2D rectangle 
         = fontMetrics.getStringBounds(text, getGraphics());

      double factor = Math.min(
            labelWidth/(rectangle.getWidth()+4.0),
            labelHeight/rectangle.getHeight());

      float newPointSize = (float) (currentPointSize * factor);

      do {
         font = font.deriveFont(newPointSize);

         // double check the width
         fontMetrics = getFontMetrics(font);
         rectangle = fontMetrics.getStringBounds(text, getGraphics());

      } while (labelWidth < rectangle.getWidth()
            && --newPointSize > 0);

      setFont(font);
   }


   /**
    * Resize font to max size. 
    */
   static class LabelResized extends ComponentAdapter {

      public void componentResized(ComponentEvent e) {
         ((MaxSizeLabel)e.getComponent()).resize();
      }
   }

}

