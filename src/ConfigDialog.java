/*  *************************************************************************
 ** Stopwatch is a fullscreen stopwatch and countdown timer.
 ** 
 ** Copyright (C) 2005 Jess Thrysoee
 ** 
 ** This program is free software; you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation; either version 2 of the License, or
 ** (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 ** GNU General Public License for more details.
 ** 
 ** You should have received a copy of the GNU General Public License
 ** along with this program; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 **
 ** *************************************************************************/

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;
import javax.swing.BorderFactory; 
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.text.JTextComponent;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.JOptionPane;


/**
 * Stopwatch configuration dialog
 */
public class ConfigDialog extends JDialog {

   JFrame stopwatchFrame;
   Stopwatch stopwatchPane;

   SelectOnFocusSpinner hoursSpinner,
                        minutesSpinner,
                        secsSpinner;

   JComboBox fontComboBox;

   ColorChooserButton foregroundButton,
                      backgroundButton;
 
   JCheckBox muteCheckBox;
   JTextField urlTextField;

   AudioHandler tmpAudio;
   String tmpAudioURLString;


   /**
    * Constructor
    */
   public ConfigDialog(JFrame frame, Stopwatch stopwatch) {
      super(frame, false);

      stopwatchFrame = frame;
      stopwatchPane = stopwatch;

      setTitle(stopwatchPane.appName + ": Preferences");

      JPanel contentPane = new JPanel(new GridBagLayout());
      GridBagConstraints c = new GridBagConstraints();
 
      c.fill = GridBagConstraints.HORIZONTAL;

      c.insets = new Insets(11,11,0,11);
      c.gridx = 0;
      c.gridy = 0;
      contentPane.add(startTimePane(), c);

      c.insets = new Insets(0,11,0,11);
      c.gridx = 0;
      c.gridy = 1;
      c.weightx = 1.0;
      c.fill = GridBagConstraints.BOTH;
      contentPane.add(stylePane(), c);

      c.insets = new Insets(0,11,11,11);
      c.gridx = 0;
      c.gridy = 2;
      c.weightx = 1.0;
      c.fill = GridBagConstraints.BOTH;
      contentPane.add(soundPane(), c);

      c.insets = new Insets(0,11,6,11);
      c.gridx = 0;
      c.gridy = 3;
      c.fill = GridBagConstraints.HORIZONTAL;
      contentPane.add(new JSeparator(), c);

      c.insets = new Insets(0,11,11,11);
      c.gridx = 0;
      c.gridy = 4;
      c.fill = GridBagConstraints.NONE;
      c.anchor = GridBagConstraints.LAST_LINE_END;
      contentPane.add(buttonPane(), c);
 
      // Set defaults or Prefs saved values
      restoreLastSavedValues();

      //Make it the content pane.
      contentPane.setOpaque(true);
      setContentPane(contentPane);

      pack();
   }


   /**
    * input start time
    */
   JPanel startTimePane() {

      JPanel pane = new JPanel(new GridBagLayout());
      pane.setBorder(BorderFactory.createTitledBorder("Start Time"));

      GridBagConstraints c = new GridBagConstraints();
      c.fill = GridBagConstraints.HORIZONTAL;

      MultiLineLabel label = new MultiLineLabel(
            "If 0:0:0 count up (stopwatch), otherwise count down.");
      c.insets = new Insets(0,6,6,6);
      c.gridx = 0;
      c.gridy = 0;
      c.weightx = 1.0;
      c.gridwidth = GridBagConstraints.REMAINDER;
      pane.add(label, c);
      c.weightx = 0.0;
      c.gridwidth = 1;

      c.fill = GridBagConstraints.HORIZONTAL;

      JLabel countDownLabel = new JLabel("Start Time (H:M:S) :");
      c.insets = new Insets(0,6,6,6);
      c.gridx = 0;
      c.gridy = 1;
      c.weightx = 1.0;
      pane.add(countDownLabel, c);

      // Hours Spinner
      hoursSpinner = new SelectOnFocusSpinner(new SpinnerNumberModel(new Long(0), new Long(0), new Long(99), new Long(1)));
      c.insets = new Insets(0,0,6,3);
      c.gridx = 1;
      c.gridy = 1;
      c.weightx = 0.0;
      pane.add(hoursSpinner, c);

      c.gridx = 2;
      c.gridy = 1;
      pane.add(new JLabel(":"), c);

      // Minutes Spinner
      minutesSpinner = new SelectOnFocusSpinner(new SpinnerNumberModel(new Long(0), new Long(0), new Long(60), new Long(1)));
      c.gridx = 3;
      c.gridy = 1;
      pane.add(minutesSpinner, c);

      c.gridx = 4;
      c.gridy = 1;
      pane.add(new JLabel(":"), c);

      // Seconds Spinner
      secsSpinner = new SelectOnFocusSpinner(new SpinnerNumberModel(new Long(0), new Long(0), new Long(60), new Long(1)));
      c.insets = new Insets(0,0,6,6);
      c.gridx = 5;
      c.gridy = 1;
      pane.add(secsSpinner, c);

      return pane;
   }


   /**
    * change style pane - font, foreground- and background color.
    */
   JPanel stylePane() {

      JPanel pane = new JPanel(new GridBagLayout());
      pane.setBorder(BorderFactory.createTitledBorder("Style"));

      GridBagConstraints c = new GridBagConstraints();

      c.fill = GridBagConstraints.HORIZONTAL;

      // Font
      c.insets = new Insets(0,6,6,6);
      c.gridx = 0;
      c.gridy = 0;
      c.weightx = 1.0;
      pane.add(new JLabel("Font:"), c);

      // Font ComboBox
      fontComboBox = new JComboBox(
            GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames());
      c.insets = new Insets(0,0,6,6);
      c.gridx = 1;
      c.gridy = 0;
      c.weightx = 0.0;
      pane.add(fontComboBox, c);

      // Color
      c.insets = new Insets(0,6,6,6);
      c.gridx = 0;
      c.gridy = 1;
      pane.add(new JLabel("Color:"), c);

      // Foreground color button
      foregroundButton =
         new ColorChooserButton("Foreground",
               stopwatchPane.appName + ": Foreground Color",
               stopwatchPane.getCurForeground());
      c.insets = new Insets(0,0,3,6);
      c.gridx = 1;
      c.gridy = 1;
      pane.add(foregroundButton, c);

      // Background color button
      backgroundButton =
         new ColorChooserButton("Background",
               stopwatchPane.appName + ": Background Color",
               stopwatchPane.getCurBackground());
      c.insets = new Insets(0,0,6,6);
      c.gridx = 1;
      c.gridy = 2;
      pane.add(backgroundButton, c);

      return pane;
   }


   /**
    * Count down sound pane
    */
   JPanel soundPane() {

      JPanel pane = new JPanel(new GridBagLayout());
      pane.setBorder(BorderFactory.createTitledBorder("Sound"));

      GridBagConstraints c = new GridBagConstraints();

      c.fill = GridBagConstraints.HORIZONTAL;

      // Mute
      muteCheckBox = new JCheckBox("Play sound when count down reach zero.");
      c.insets = new Insets(0,6,6,6);
      c.gridx = 0;
      c.gridy = 0;
      c.weightx = 0.0;
      c.gridwidth = GridBagConstraints.REMAINDER;
      pane.add(muteCheckBox, c);
      c.gridwidth = 1;


      // URL field
      urlTextField = new JTextField(10);
      c.insets = new Insets(0,6,6,0);
      c.fill = GridBagConstraints.BOTH;
      c.gridx = 0;
      c.gridy = 1;
      c.weightx = 1.0;
      pane.add(urlTextField, c);

      // JFileChooser button
      c.insets = new Insets(0,0,6,0);
      c.fill = GridBagConstraints.NONE;
      c.gridx = 1;
      c.gridy = 1;
      c.weightx = 0.0;
      pane.add(browseButton(), c);

      // Play button
      c.fill = GridBagConstraints.VERTICAL;
      c.insets = new Insets(0,2,6,6);
      c.gridx = 2;
      c.gridy = 1;
      c.weightx = 0.0;
      pane.add(playButton(), c);

      return pane;
   }


   /**
    * Ok, Apply and Cancel button pane
    */
   JPanel buttonPane() {

      // GridLayout ensure equal size of buttons
      JPanel pane = new JPanel(new GridLayout(1, 2, 6, 0));

      pane.add(okButton());
      pane.add(applyButton());
      pane.add(cancelButton());

      return pane;
   }


   /**
    * OK Button
    */
   private JButton okButton() {

      JButton b = new JButton("OK");
      b.addActionListener(
            new ActionListener() {
               public void actionPerformed(ActionEvent e) {
                  if (saveAndUpdate()) {
                     setVisible(false);
                  }
               }
            });

      return b;
   }


   /**
    * Apply Button
    */
   private JButton applyButton() {

      JButton b = new JButton("Apply");
      b.addActionListener(
            new ActionListener() {
               public void actionPerformed(ActionEvent e) {
                  saveAndUpdate();
               }
            });

      return b;
   }


   /**
    * Cancel Button
    */
   private JButton cancelButton() {

      JButton b = new JButton("Cancel");
      b.addActionListener(
            new ActionListener() {
               public void actionPerformed(ActionEvent e) {
                  // revert to last saved values
                  restoreLastSavedValues();

                  setVisible(false);
               }
            });

      return b;
   }


   /**
    * Browse Button
    */
   private JButton browseButton() {

      final JFileChooser fileChooser = new JFileChooser();

      JButton b = new JButton("Browse");
      b.addActionListener(
            new ActionListener() {
               public void actionPerformed(ActionEvent e) {
                  int res = fileChooser.showOpenDialog((Component)e.getSource());

                  if (res == JFileChooser.APPROVE_OPTION) {
                     urlTextField.setText(fileChooser.getSelectedFile().toURI().toString());
                  }

               }
            });

      return b;
   }


   /**
    * Play Button
    */
   private PlayButton playButton() {

      PlayButton b = new PlayButton();

      b.addActionListener(
            new ActionListener() {
               public void actionPerformed(ActionEvent e) {
                  updateTmpAudio();
                  tmpAudio.play();
               }
            });

      return b;
   }

   /**
    * update tmp Audio
    */
   private boolean updateTmpAudio() {

      URL url;
      String urlString = urlTextField.getText();

      try {
         if (urlString.trim().length() == 0) {
            url = null;
         } else {
            url = new URL(urlString);
         }
      } catch (MalformedURLException e) {

         String msg = new String(e.getMessage()
               +"\n"
               +"\nURL examples:"
               +"\n'file:/home/user/sound.wav'"
               +"\n'http://www.example.org/sound.wav'");

         JOptionPane.showMessageDialog(this, msg, "Malformed Sound URL",
               JOptionPane.ERROR_MESSAGE);
         return false;
      }

      if (!urlString.equals(tmpAudioURLString)) {
         if (tmpAudio == null) {
            // initialize Audio 
            tmpAudio = new AudioHandler();
            stopwatchPane.getCurAudio(tmpAudio);
         } else {
            // load new Audio 
            tmpAudio.load(url);
         }
         tmpAudioURLString = urlString;
      }

      return true;
   }


   /**
    * Save selected values and update stopwatchPane
    */
   private boolean saveAndUpdate() {

      // save current values and update app
      stopwatchPane.setCurBackground(backgroundButton.getColor());
      stopwatchPane.setCurForeground(foregroundButton.getColor());
      stopwatchPane.setCurFontFamily((String)fontComboBox.getSelectedItem(), true);
      stopwatchPane.setStartSecond(getSpinnerTime());
      stopwatchPane.setCurAudioMute(muteCheckBox.isSelected());

      if (!updateTmpAudio()) {
         return false;
      }
      stopwatchPane.setCurAudioURLString(tmpAudioURLString);
      stopwatchPane.setCurAudio(tmpAudio);

      stopwatchFrame.repaint();
      return true;
   }

   /**
    * get and set last saved values from stopwatch
    */
   private void restoreLastSavedValues() {

      foregroundButton.setColor(stopwatchPane.getCurForeground());
      backgroundButton.setColor(stopwatchPane.getCurBackground());
      fontComboBox.setSelectedItem(stopwatchPane.getCurFontFamily());
      setSpinnerTime();
      muteCheckBox.setSelected(stopwatchPane.getCurAudioMute());
      urlTextField.setText(stopwatchPane.getCurAudioURLString());
      updateTmpAudio();
   }


   /**
    * Get start time from spinners
    */
   public long getSpinnerTime() {

      long hours = ((SpinnerNumberModel) hoursSpinner.getModel()).getNumber().longValue();
      long minutes = ((SpinnerNumberModel) minutesSpinner.getModel()).getNumber().longValue();
      long secs = ((SpinnerNumberModel) secsSpinner.getModel()).getNumber().longValue();

      return 3600*hours + 60*minutes + secs;
   }


   /**
    * Set start time on spinners
    */
   public void setSpinnerTime() {

      long seconds = stopwatchPane.getStartSecond();

      long hours = seconds/3600;
      long minutes = (seconds%3600)/60;
      long secs = (seconds%3600)%60;

      ((SpinnerNumberModel) hoursSpinner.getModel()).setValue(new Long(hours));
      ((SpinnerNumberModel) minutesSpinner.getModel()).setValue(new Long(minutes));
      ((SpinnerNumberModel) secsSpinner.getModel()).setValue(new Long(secs));
   }


   /**
    * Close dialog with Escape button
    */
   protected JRootPane createRootPane() {
      ActionListener actionListener = new ActionListener() {
         public void actionPerformed(ActionEvent actionEvent) {
            setVisible(false);
         }
      };
      JRootPane rootPane = new JRootPane();
      KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
      rootPane.registerKeyboardAction(actionListener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
      return rootPane;
   }

}

