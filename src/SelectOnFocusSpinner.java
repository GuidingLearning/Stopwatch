/*  *************************************************************************
 ** Stopwatch is a fullscreen stopwatch and countdown timer.
 ** 
 ** Copyright (C) 2005 Jess Thrysoee
 ** 
 ** This program is free software; you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation; either version 2 of the License, or
 ** (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 ** GNU General Public License for more details.
 ** 
 ** You should have received a copy of the GNU General Public License
 ** along with this program; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 **
 ** *************************************************************************/

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;

/**
 * JSpinner selectAll textComponent on focus
 */
public class SelectOnFocusSpinner extends JSpinner {

   /**
    * Constructor
    */
   public SelectOnFocusSpinner(SpinnerNumberModel model) {
      super(model);

      setEditor(new JSpinner.NumberEditor(this, "#"));

      // select all text on focus
      ((JSpinner.DefaultEditor)getEditor()).getTextField().addFocusListener(new FocusAdapter() {
         public void focusGained(FocusEvent e) {
            if (e.getSource() instanceof JTextComponent) {
               final JTextComponent textComponent = (JTextComponent)e.getSource();
               SwingUtilities.invokeLater(new Runnable() {
                  public void run() {
                     textComponent.selectAll();
                  }
               });
            }
         }
         public void focusLost(FocusEvent e) { }
      });

   }
}
