/*  *************************************************************************
 ** Stopwatch is a fullscreen stopwatch and countdown timer.
 ** 
 ** Copyright (C) 2005 Jess Thrysoee
 ** 
 ** This program is free software; you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation; either version 2 of the License, or
 ** (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 ** GNU General Public License for more details.
 ** 
 ** You should have received a copy of the GNU General Public License
 ** along with this program; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 **
 ** *************************************************************************/

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JColorChooser;


/**
 * Button activates JColorChooser and shows selected color in its icon.
 */
public class ColorChooserButton extends JButton {

   private String title;
   private Color color;

   public ColorChooserButton(String text, String dialogTitle, Color defaultColor) {
      super(text);

      this.title = dialogTitle;
      this.color = defaultColor;

      setIcon(new ColorIcon());

      addActionListener(
            new ActionListener() {
               public void actionPerformed(ActionEvent e) {

                  Color res = JColorChooser.showDialog((Component)e.getSource(), title, color);

                  if (res != null) {
                     color = res;
                  }
               }
            });
   }


   /**
    * return the Icon color
    */
   public Color getColor() {
      return color;
   }


   /**
    * set Icon color
    */
   public void setColor(Color color) {
      this.color = color;
   }


   /**
    * paint icons as square in current selected color
    */
   class ColorIcon implements Icon { 

      public int getIconWidth() { 
         return 11; 
      } 

      public int getIconHeight() { 
         return 11; 
      } 

      public void paintIcon(Component c, Graphics g, int x, int y) { 
         g.setColor(Color.black); 
         g.fillRect(x, y, getIconWidth(), getIconHeight()); 
         g.setColor(color);
         g.fillRect(x+2, y+2, getIconWidth()-4, getIconHeight()-4); 
      } 
   }

}

