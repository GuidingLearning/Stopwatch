/*  *************************************************************************
 ** Stopwatch is a fullscreen stopwatch and countdown timer.
 ** 
 ** Copyright (C) 2005 Jess Thrysoee
 ** 
 ** This program is free software; you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation; either version 2 of the License, or
 ** (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 ** GNU General Public License for more details.
 ** 
 ** You should have received a copy of the GNU General Public License
 ** along with this program; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 **
 ** *************************************************************************/

import java.net.URL;
import java.applet.Applet;
import java.applet.AudioClip;

/**
 * asynchronous loading AudioClip wrapper class
 */
public class AudioHandler {

   private AudioClip audioClip;
   private boolean available = false;

   private Object lock = new Object();

   /**
    * play if AudioClip is available
    */
   public void play() {
      new Thread() {
         public void run() {
            synchronized(lock) {
               while (available == false) {
                  try {
                     lock.wait(); // wait for clip to finish loading
                  } catch (InterruptedException e) { }
               }
               audioClip.play();
            }
         }
      }.start();
   }


   /**
    * blocking load of new AudioClip
    */
   public void load(final URL url) {
      new Thread() {
         public void run() {
            synchronized(lock) {
               audioClip = Applet.newAudioClip(url);
               available = true;
               lock.notifyAll();
            }
         }
      }.start();
   }


   /**
    * copy this AudioClip to 'dst' 
    */
   public void copyTo(final AudioHandler dst) {
      new Thread() {
         public void run() {
            synchronized(lock) {
               while (available == false) {
                  try {
                     lock.wait(); // wait for clip to finish loading
                  } catch (InterruptedException e) { }
               }
               dst.setAudioClip(audioClip);
            }
         }
      }.start();
   }

   /**
    * Accessor assign new AudioClip
    */
   private void setAudioClip(AudioClip clip) {
      synchronized(lock) {
         audioClip = clip;
         available = true;
         lock.notifyAll();
      }
   }

}

